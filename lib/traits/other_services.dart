
import 'dart:convert';
import 'package:http/http.dart' as http;
class OtherServices{

  getProfileInfo(String accessToken) async{

    Map responseData;
    Map<String, String> headers = {
      "Content-type": "application/x-www-form-urlencoded",
      "Authorization": "Bearer $accessToken"
      //"token": "a1b2c3d4e5"
    };
    try{
      http.Response response = await http
          .get('https://api.truelayer.com/data/v1/info', headers: headers);
      responseData = json.decode(response.body);
      responseData = responseData['results'][0];
      //print(responseData.toString());
      return responseData;
    }catch(e){
      print("Error" + e);
    }
    //return responseData;
  }

  getBankInfo(String accessToken) async{

    Map responseData;
    //print(this.dataClient['access_token']);
    Map<String, String> headers = {
      "Content-type": "application/x-www-form-urlencoded",
      "Authorization": "Bearer $accessToken"
      //"token": "a1b2c3d4e5"
    };
    try{
    http.Response response = await http
        .get('https://api.truelayer.com/data/v1/me', headers: headers);
    responseData = json.decode(response.body);
    return responseData['results'][0]['provider'];
    //responseData = responseData['results'];
    //print("cards:" + cards.toString());
    }catch(e){

    }
  }
}