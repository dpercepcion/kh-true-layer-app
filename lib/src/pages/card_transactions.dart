import 'dart:async';
import 'package:bank/traits/user_data.dart';
import 'package:bank/widgets/loader.dart';
import 'package:flutter/material.dart';
import 'package:bank/traits/card_services.dart';

class CardTransactions extends StatefulWidget {
  CardTransactions({
    Key key,
  }) : super(key: key);
  static String cardId = "";
  @override
  _CardTransactionsState createState() => _CardTransactionsState();
}

class _CardTransactionsState extends State<CardTransactions> {
  Map dataClient;
  List cardTransactions;
  Map cardBalance;
  BuildContext globalContext;
  bool loaderState = true;
  Color hexToColor(String code) {
    // convert hexa color to flutter color
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  Future fetchCardTransactions() async {
    dataClient = await new UserData().getData();
      cardTransactions = await new CardServices().getCardTransactions(dataClient['access_token'], CardTransactions.cardId);
      cardBalance = await new CardServices()
          .getCardBalance(dataClient['access_token'], CardTransactions.cardId);
      loaderState = false;
  }

  @override
  Widget build(BuildContext context) {
    globalContext = context;
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Transactions"),
          backgroundColor: hexToColor('#FF6060'),
        ),
        body: FutureBuilder(
        future: fetchCardTransactions(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (this.loaderState == false) {
            return listCardTransactions();
          }
          return Loader();
        },
      )
      ),
    );
  }
  Widget listCardTransactions(){
    return Container(
      padding: EdgeInsets.all(5),
      child: ListView.builder(
        itemCount: this.cardTransactions == null
            ? 0
            : this.cardTransactions.length,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            children: <Widget>[
              Card(
                  child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "${this.cardTransactions[index]['timestamp'].substring(0, 10)}",
                          textScaleFactor: 1.2,
                        ),
                        Text(
                          "${this.cardTransactions[index]['transaction_type']}",
                          textScaleFactor: 1.2,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "${this.cardTransactions[index]['description']}",
                          textScaleFactor: 1.2,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          "${this.cardTransactions[index]['currency']} ${this.cardTransactions[index]['amount']}",
                          style: TextStyle(
                              color: (this.cardTransactions[index]
                                          ['amount'] >
                                      0)
                                  ? Colors.green
                                  : Colors.red),
                          textScaleFactor: 1.3,
                        ),
                      ],
                    ),
                  ),
                ],
              ))
            ],
          );
        }
      )
    );
  }
}
