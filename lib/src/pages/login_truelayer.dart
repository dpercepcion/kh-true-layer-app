import 'dart:async';
import 'package:bank/widgets/loader.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class LoginTruelayer extends StatefulWidget {
  LoginTruelayer({Key key}) : super(key: key);

  @override
  _LoginTruelayerState createState() => _LoginTruelayerState();
}

class _LoginTruelayerState extends State<LoginTruelayer> {

  RoundedRectangleBorder shapeButton = new RoundedRectangleBorder(
      borderRadius: new BorderRadius.circular(10.0)); //buttons shape

  Color hexToColor(String code) {
    // convert hexa color to flutter color
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }
  WebViewController webController;
  String urlTrueLayer = "https://auth.truelayer.com/?response_type=code&client_id=docentecesde2019-679b35&scope=info%20accounts%20balance%20cards%20transactions%20direct_debits%20standing_orders%20offline_access&redirect_uri=https://console.truelayer.com/redirect-page&enable_mock=true&disable_providers=uk-oauth-all";
  BuildContext globalContext;
  bool showWebView = false;
  bool showLoader = true;

  @override
  Widget build(BuildContext context) {
    globalContext = context;
    return Scaffold(
      appBar: AppBar(
        title: Text('Bank'),
        backgroundColor: hexToColor('#FF6060'),
        // This drop down menu demonstrates that Flutter widgets can be shown over the web view.
      ),
      body: Stack(
        children: <Widget>[
          Opacity(
            opacity: 1.0,
            child: WebView(
              initialUrl:urlTrueLayer,
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (WebViewController webViewController) {
                webController = webViewController;
              },
              onPageFinished: (url){
                print("URL: " + url);
                setState(() {
                  showLoader = false;
                  showWebView = true;
                });
                onPageFinished(url);
              },
            ),
          ),
          Visibility(
            visible: showLoader,
            child: Loader(),
          )
        ],
      )
      /* WebView(
        initialUrl:urlTrueLayer,
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController) {
          webController = webViewController;
        },
        onPageFinished: (url){
          print("URL: " + url);
          onPageFinished(url);
        },
      ), */
    );
  }

  void onPageFinished(String url) {
    String userCode = "";
    if (url.contains('code=')) {
      int codePosition = url.indexOf('code=');
      while (true) {
        if (url[codePosition] == '&') {
          break;
        } else {
          userCode = userCode + url[codePosition];
          codePosition++;
        }
      }
      userCode = userCode.substring(5);
      //print("code: " + userCode);
      getAccesToken(userCode);
    }
    else if(url == "https://console.truelayer.com/auth"){
      _showDialog("Something was wrong, try login again");
      Navigator.of(context).pushNamedAndRemoveUntil('/truelayer', (Route<dynamic> route) => false); //navigate to other screen and remove all previous
    }
  }
  void getAccesToken(String userCode) async {
    try{
      Map responseData;
      Map userData = {
        "grant_type": "authorization_code",
        "client_id": "docentecesde2019-679b35",
        "client_secret": "d77999d5-1b2d-4c35-8589-52c47464ab55",
        "redirect_uri": "https://console.truelayer.com/redirect-page",
        "code": "$userCode",
      };
      Map<String, String> headers = {
        "Content-type": "application/x-www-form-urlencoded",
        //"token": "a1b2c3d4e5"
      };
      http.Response response = await http.post(
          'https://auth.truelayer.com/connect/token',
          headers: headers,
          body: userData);
      responseData = json.decode(response.body);
      //print("AccesToken: " + responseData['access_token']);
      saveApiData(userCode, responseData['access_token'], responseData['refresh_token'], getAndformatDate());
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      Navigator.pushReplacementNamed(context, '/dashboard');
    }catch(e){
      _showDialog("Something was wrong, try again");
    }
    //retrieveIdentityInformation(responseData['access_token']);
  }

  void _showDialog(String message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: Center(
              child: Text(
            "Alert",
            style: TextStyle(color: hexToColor('#FF6060')),
            textScaleFactor: 1.3,
          )),
          content: Container(
            width: 100,
            height: 130,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Center(
                    child: Text(
                  message,
                  textAlign: TextAlign.center,
                )),
                Container(
                  padding: EdgeInsets.all(20),
                  width: 150,
                  //height: 100,
                  child: RaisedButton(
                    shape: shapeButton,
                    padding: EdgeInsets.all(10),
                    color: Colors.green,
                    child: Text(
                      "Ok",
                      textScaleFactor: 1.4,
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void saveApiData(String code, String accessToken, String refreshToken, String date) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('code', "$code");
    prefs.setString('access_token', "$accessToken");
    prefs.setString('refresh_token', "$refreshToken");
    prefs.setString('client_secret', "d77999d5-1b2d-4c35-8589-52c47464ab55");
    prefs.setString('client_id', "docentecesde2019-679b35");
    prefs.setString('date', "$date");
    prefs.setString('login', "success");
  }

  void retrieveIdentityInformation(String token) async {
    Map<String, String> headers = {
      "Content-type": "application/x-www-form-urlencoded",
      "Authorization": "Bearer $token"
      //"token": "a1b2c3d4e5"
    };
    http.Response response = await http
        .get('https://api.truelayer.com/data/v1/info', headers: headers);
    print(response.body.toString());
    //responseData = json.decode(response.body);
  }


  String getAndformatDate(){
    var now = new DateTime.now();
    String year = now.toString().substring(0,4);
    String month = now.toString().substring(5,7);
    String day = now.toString().substring(8,10);
    String hour = now.toString().substring(11,13);
    String minute = now.toString().substring(14,16);
    return year + month + day + hour + minute;
  }

  getLastDate() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String date = prefs.getString('client_id').toString();
    return date;
  }
  Future<Map> getDataClient() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //String client_id = prefs.getString('client_id');
    //String client_secret = prefs.getString('client_secret');
    //String code = prefs.getString('code');
    //String refresh_token = prefs.getString('refresh_token');
    Map data = {
      "client_id": "${prefs.getString('client_id')}",
      "client_secret": "${prefs.getString('client_secret')}",
      "code": "${prefs.getString('code')}",
      "refresh_token": "${prefs.getString('refresh_token')}",
    };
    return data;
  }
}
