
import 'dart:convert';
import 'package:http/http.dart' as http;
class AccountServices{

  getAccountBalance(String accessToken, String accountId) async{

    Map responseData;
    Map<String, String> headers = {
      "Content-type": "application/x-www-form-urlencoded",
      "Authorization": "Bearer $accessToken"
      //"token": "a1b2c3d4e5"
    };
    try{
      http.Response response = await http
          .get('https://api.truelayer.com/data/v1/accounts/$accountId/balance', headers: headers);
      responseData = json.decode(response.body);
      responseData = responseData['results'][0];
      print(responseData.toString());
      return responseData;
    }catch(e){
      print("Error" + e);
    }
    //return responseData;
  }

  getListAllAccounts(String accessToken) async{

    Map responseData;
    //print(this.dataClient['access_token']);
    Map<String, String> headers = {
      "Content-type": "application/x-www-form-urlencoded",
      "Authorization": "Bearer $accessToken"
      //"token": "a1b2c3d4e5"
    };
    try{
    http.Response response = await http
        .get('https://api.truelayer.com/data/v1/accounts', headers: headers);
    responseData = json.decode(response.body);
    return responseData['results'];
    //responseData = responseData['results'];
    //print("accounts:" + accounts.toString());
    }catch(e){

    }
  }
  getAccountTransactions(String accessToken, String accountId) async{
    Map responseData;
    //print(this.dataClient['access_token']);
    Map<String, String> headers = {
      "Content-type": "application/x-www-form-urlencoded",
      "Authorization": "Bearer $accessToken"
      //"token": "a1b2c3d4e5"
    };
    try{
      http.Response response = await http
          .get('https://api.truelayer.com/data/v1/accounts/$accountId/transactions', headers: headers);
      responseData = json.decode(response.body);
      //print(responseData.toString());
      return responseData['results'];
    }catch(e){
      print(e);
    }
    //return responseData;
  }

}