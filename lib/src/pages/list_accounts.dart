
import 'dart:async';

import 'package:bank/src/pages/account_transactions.dart';
import 'package:bank/widgets/loader.dart';
import 'package:flutter/material.dart';
import 'package:bank/traits/account_services.dart';
import 'package:bank/traits/user_data.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:webview_flutter/webview_flutter.dart';
class ListAccounts extends StatefulWidget {
  ListAccounts({Key key}) : super(key: key);
  @override
  _ListAccountsState createState() => _ListAccountsState();
}

class _ListAccountsState extends State<ListAccounts> {

  RoundedRectangleBorder shapeButton = new RoundedRectangleBorder(
      borderRadius: new BorderRadius.circular(10.0)); //buttons shape

  Color hexToColor(String code) {
    // convert hexa color to flutter color
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  WebViewController controller;
  String displayName = "";
  String logoUri = "";
  Map dataClient;
  String userCode = "";
  bool loaderState = true;
  List userAccounts;

  Future fetchListAcounts() async {
    this.dataClient = await UserData().getData();
      //print("Access Token:" + dataClient['access_token']);
      String currentDate = getAndformatDate();
      //print("currentDate: " + currentDate + " LastDate:" + dataClient['date'].toString());
      if(int.parse(currentDate) - int.parse(dataClient['date']) >= 60){
        await renewToken();
        await retrieveAccessTokenMetadata();
        userAccounts = await AccountServices().getListAllAccounts(dataClient['access_token']);
      }
      else{
        await retrieveAccessTokenMetadata();
        userAccounts = await AccountServices().getListAllAccounts(dataClient['access_token']);
      }
      loaderState = false;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Accounts"),
        backgroundColor: hexToColor('#FF6060'),
      ),
      body: FutureBuilder(
        future: fetchListAcounts(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (this.loaderState == false) {
            return listAccounts();
          }
          return Loader();
        },
      )
    );
  }

  Widget listAccounts(){
    return ListView.builder(

      itemCount: this.userAccounts == null ? 0 : this.userAccounts.length,
      itemBuilder: (BuildContext context, int index){
        return Container(
          padding: EdgeInsets.all(5),
          child: GestureDetector(
            onTap: () async{
              AccountTransactions.accountId = this.userAccounts[index]['account_id'];
              Map accountBalace = await AccountServices().getAccountBalance(this.dataClient['access_token'],this.userAccounts[index]['account_id']);
              _showAccountBalance(accountBalace);
            },
          //getAccountBalance(this.userAccounts[index]['account_id']),
            child: Card(
              color: hexToColor('#284e7f'),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(10),
                        child: Text(
                          "Account", 
                          textScaleFactor: 1.5, 
                          style: TextStyle(
                            color: Colors.white
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(10),
                        child: Text(
                          "${this.userAccounts[index]['account_number']['number']}", 
                          textScaleFactor: 1.4,
                          style: TextStyle(
                            color: Colors.white
                          ),
                        )
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(10),
                        child: Text(
                          "${this.userAccounts[index]['display_name']}", 
                          textScaleFactor: 1.5,
                          style: TextStyle(
                            color: Colors.white
                          ),
                        ),
                      ),
                      //Text("${this.userAccounts[index]['account_number']['number']}");
                    ],
                  )
                ],
              ),
            )
          ),
        );
      },
    );
  }
  Future<void> renewToken() async {
    try{
      //print("trying to renew");
      this.dataClient["grant_type"] = "refresh_token";
      //print("dataClient: " + dataClient.toString());
      Map<String, String> headers = {
        "Content-type": "application/x-www-form-urlencoded",
        //"Authorization": "Bearer $token"
      };
      http.Response response = await http.post(
          'https://auth.truelayer.com/connect/token',
          headers: headers, body: dataClient,
          );
      Map accessToken = json.decode(response.body);
      //print("accesstoken: " + accessToken.toString());
      await saveAccesToken(accessToken['access_token']);
    }catch(e){
      _showDialog("Error, try again");
    }
  }

  void _showAccountBalance(Map dataAccount){
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: Center(
              child: Text(
            "Account Balance",
            style: TextStyle(color: hexToColor('#FF6060')),
            textScaleFactor: 1.3,
          )),
          content: Container(
            width: MediaQuery.of(context).size.width*0.95,
            child: SingleChildScrollView(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 1)
                    )
                  ),
                  child: Row(
                    children: <Widget>[
                      Text("Currency"),
                      Spacer(), // use Spacer
                      Text("${dataAccount['currency']}"),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 1)
                    )
                  ),
                  child: Row(
                    children: <Widget>[
                      Text("Available"),
                      Spacer(), // use Spacer
                      Text("${dataAccount['available']}"),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 1)
                    )
                  ),
                  child: Row(
                    children: <Widget>[
                      Text("Current"),
                      Spacer(), // use Spacer
                      Text("${dataAccount['current']}"),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 1)
                    )
                  ),
                  child: Row(
                    children: <Widget>[
                      Text("Overdraft"),
                      Spacer(), // use Spacer
                      Text("${(dataAccount['overdraft'] != null) ? dataAccount['overdraft'] : ''}"),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.only(top: 5),
                  width: 180,
                  //height: 100,
                  child: RaisedButton(
                    shape: shapeButton,
                    padding: EdgeInsets.all(15),
                    color: hexToColor('#FF6060'),
                    child: Text(
                      "Transactions",
                      textScaleFactor: 1.4,
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      //AccountTransactions.accountId = dataAccount['account_id'];
                      Navigator.pushNamed(context, '/accounttransactions');
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  width: 180,
                  //height: 100,
                  child: RaisedButton(
                    shape: shapeButton,
                    padding: EdgeInsets.all(10),
                    color: hexToColor('#284e7f'),
                    child: Text(
                      "Close",
                      textScaleFactor: 1.4,
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            ),
            )
          ),
        );
      },
    );
  }

  void _showDialog(String message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: Center(
              child: Text(
            "Alert",
            style: TextStyle(color: hexToColor('#FF6060')),
            textScaleFactor: 1.3,
          )),
          content: Container(
            width: 100,
            height: 130,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Center(
                    child: Text(
                  message,
                  textAlign: TextAlign.center,
                )),
                Container(
                  padding: EdgeInsets.all(20),
                  width: 150,
                  //height: 100,
                  child: RaisedButton(
                    shape: shapeButton,
                    padding: EdgeInsets.all(10),
                    color: Colors.green,
                    child: Text(
                      "Ok",
                      textScaleFactor: 1.4,
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future<void> saveAccesToken(String accessToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('access_token', "$accessToken");
    prefs.setString('date', getAndformatDate());
    setState(() {
      this.dataClient['access_token'] = accessToken;
      this.dataClient['date'] = getAndformatDate();
    });
  }

  Future<void> retrieveAccessTokenMetadata() async{
    Map responseData;
    //print(this.dataClient['access_token']);
    Map<String, String> headers = {
      "Content-type": "application/x-www-form-urlencoded",
      "Authorization": "Bearer ${this.dataClient['access_token']}"
      //"token": "a1b2c3d4e5"
    };
    http.Response response = await http
        .get('https://api.truelayer.com/data/v1/me', headers: headers);
    responseData = json.decode(response.body);
    responseData = responseData['results'][0]['provider'];
    if (!mounted) return;
    setState(() {
      this.displayName = responseData['display_name'].toString();
      this.logoUri = responseData['logo_uri'].toString();
      //this.controller.loadUrl(this.logoUri);
    });
    //print("Display Name: " + displayName + " logo uri:" + logoUri);
  }

  String getAndformatDate(){
    var now = new DateTime.now();
    String year = now.toString().substring(0,4);
    String month = now.toString().substring(5,7);
    String day = now.toString().substring(8,10);
    String hour = now.toString().substring(11,13);
    String minute = now.toString().substring(14,16);
    return year + month + day + hour + minute;
  }
}