import 'dart:async';
import 'package:bank/traits/other_services.dart';
import 'package:bank/traits/user_data.dart';
import 'package:bank/widgets/loader.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Bank extends StatefulWidget {
  Bank() : super();
  static String cardId = "";
  @override
  _BankState createState() => _BankState();
}

class _BankState extends State<Bank> {
  Map dataClient;
  Map bankList;
  bool loaderState = true;
  BuildContext globalContext;
  Color hexToColor(String code) {
    // convert hexa color to flutter color
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  Future fetchBankInfo() async {
    dataClient = await new UserData().getData();
    bankList =
        await new OtherServices().getBankInfo(dataClient['access_token']);
    loaderState = false;
  }

  @override
  Widget build(BuildContext context) {
    globalContext = context;
    return Scaffold(
        appBar: AppBar(
          title: Text("Cards"),
          backgroundColor: hexToColor('#FF6060'),
        ),
        body: FutureBuilder(
          future: fetchBankInfo(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (this.loaderState == false) {
              return bankInfo();
            }
            return Loader();
          },
        ));
  }

  Widget bankInfo() {
    return Table(
      children: [
        TableRow(children: [
          Container(
            height: 150,
            child: WebView(
              initialUrl: this.bankList['logo_uri'],
              javascriptMode: JavascriptMode.unrestricted,
            ),
          ),
          Container(
            height: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text("${this.bankList['display_name']}", textScaleFactor: 1.5,),
                Text("${this.bankList['display_name']}", textScaleFactor: 1.5,),
              ],
            ),
          )
        ])
      ],
    );
  }
}
