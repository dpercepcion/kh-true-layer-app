import 'package:bank/src/pages/account_transactions.dart';
import 'package:bank/src/pages/bank.dart';
import 'package:bank/src/pages/card_transactions.dart';
import 'package:bank/src/pages/dashboard.dart';
import 'package:bank/src/pages/list_accounts.dart';
import 'package:bank/src/pages/list_cards.dart';
import 'package:bank/src/pages/login_page.dart';
import 'package:bank/src/pages/login_truelayer.dart';
import 'package:bank/src/pages/profile.dart';
import 'package:bank/src/pages/splash_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bank/src/pages/register_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  // fix android x problem
  //https://github.com/flutter/flutter/issues/27679
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Bank',
      initialRoute: '/',
      routes: {
        '/': (context) => SplashPage(),
        '/login' : (context) => LoginPage(),
        '/register' : (context) => RegisterPage(),
        '/truelayer': (context) => LoginTruelayer(),
        '/cards' : (context) => ListCards(),
        '/accounts' : (context) => ListAccounts(),
        '/cardtransactions': (context) => CardTransactions(),
        '/accounttransactions': (context) => AccountTransactions(),
        '/dashboard': (context) => Dashboard(),
        '/profile': (context) => Profile(),
        '/bank': (context) => Bank()
      },
    );
  }
}

