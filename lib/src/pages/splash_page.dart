import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
class SplashPage extends StatefulWidget {
  SplashPage({Key key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  String loginState = "";
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 4), () async{
      //Navigator.pushReplacementNamed(context, '/login');
      await getLoginState();
      if(this.loginState == "success"){
        Navigator.pushReplacementNamed(context, '/dashboard');
      }
      else{
        Navigator.pushReplacementNamed(context, '/login');
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size; // get screen size
    return Scaffold(
      body: Stack( // add widgets over one other
        children: <Widget>[
          Center(
            child: new Image.asset(
              'assets/images/london.jpg',
              width: size.width,
              height: size.height,
              fit: BoxFit.fill,
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Image.asset('assets/images/unnamed.png'),
                  width: 150,
                  height: 150,
                ),
                Text("Bank",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 28.0,
                    color: Colors.white
                  )
                ),
              ],
            )
          ),
        ],
      )
    );
  }
  Future<void> getLoginState() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    String stringValue = prefs.getString('login');
    setState(() {
      this.loginState = stringValue;
    });
    //return stringValue;
  }

}
