import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Dashboard extends StatefulWidget {
  Dashboard({Key key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    Color hexToColor(String code) {
      // convert hexa color to flutter color
      return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
    }

    return Scaffold(
        appBar: AppBar(
          title: Text("Dashboard"),
          backgroundColor: hexToColor('#FF6060'),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  logout();
                  //Navigator.pushReplacementNamed(context, '/login');
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/login', (Route<dynamic> route) => false);
                },
                child: Icon(
                  Icons.exit_to_app,
                  size: 26.0,
                ),
              ),
            )
          ],
        ),
        body: Table(
          children: [
            TableRow(children: [
              Container(
                height: 100,
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: hexToColor('#c8c8c8')),
                child: Center(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/accounts');
                    },
                    child: Text("Accounts", textScaleFactor: 1.4),
                  ),
                ),
              ),
              Container(
                height: 100,
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: hexToColor('#c8c8c8')),
                child: Center(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/cards');
                    },
                    child: Text("Cards", textScaleFactor: 1.4),
                  ),
                ),
              ),
            ]),
            TableRow(children: [
              Container(
                height: 100,
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: hexToColor('#c8c8c8')),
                child: Center(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/profile');
                    },
                    child: Text("Profile", textScaleFactor: 1.4),
                  ),
                ),
              ),
              Container(
                height: 100,
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: hexToColor('#c8c8c8')),
                child: Center(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/bank');
                    },
                    child: Text("Bank", textScaleFactor: 1.4),
                  ),
                ),
              ),
            ]),
          ],
        )
        /* Container(
        padding: EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              width: 150,
              height: 150,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: hexToColor('#c8c8c8')
              ),
              child: Center(
                child: GestureDetector(
                  onTap: ()=>{
                    Navigator.pushNamed(context, '/accounts')
                  },
                  child: Text("Accounts", textScaleFactor: 1.4),
                ),
              )
            ),
            Container(
              width: 150,
              height: 150,
              //margin: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: hexToColor('#c8c8c8')
              ),
              child: Center(
                child: GestureDetector(
                  onTap: ()=>{
                    Navigator.pushNamed(context, '/cards')
                  },
                  child: Text("Cards", textScaleFactor: 1.4),
                ),
              )
            ),
          ],
        ),
      ), */
        );
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('login', 'logout');
  }
}
