import 'package:shared_preferences/shared_preferences.dart';

class UserData{

  getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map data = {
      "client_id": "${prefs.getString('client_id')}",
      "client_secret": "${prefs.getString('client_secret')}",
      "code": "${prefs.getString('code')}",
      "refresh_token": "${prefs.getString('refresh_token')}",
      "date": "${prefs.getString('date')}",
      "access_token": "${prefs.getString('access_token')}"
    };
    return data;
  }

}