import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  RoundedRectangleBorder shapeButton = new RoundedRectangleBorder(
      borderRadius: new BorderRadius.circular(10.0)); //buttons shape

  Color hexToColor(String code) {
    // convert hexa color to flutter color
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size; // get screen size
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Stack(
                // add widgets over one other
                children: <Widget>[
                  Center(
                    child: new Image.asset(
                      'assets/images/header.jpg',
                      width: size.width,
                      height: size.height,
                      fit: BoxFit.fill,
                    ),
                  ),
                  Center(
                    child: SafeArea(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            child: Image.asset('assets/images/unnamed.png'),
                            width: 150,
                            height: 150,
                          ),
                          Text("Bank",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 28.0,
                                  color: Colors.white)),
                          Container(
                            padding: EdgeInsets.only(left: 50, right: 50, top: 20),
                            child: TextField(
                              autofocus: false,
                              controller: usernameController,
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                labelStyle: TextStyle(color: Colors.white),
                                hintText: "Username",
                                hintStyle: TextStyle(color: Colors.white),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                ),
                              ),
                              /* onTap: () {
                                SystemChannels.textInput.invokeMethod('TextInput.show');
                              }, */
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 50, right: 50, top: 20),
                            child: TextField(
                              controller: passwordController,
                              obscureText: true,
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                labelStyle: TextStyle(color: Colors.white),
                                hintText: "Password",
                                hintStyle: TextStyle(color: Colors.white),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(20),
                            width: 300,
                            //height: 100,
                            child: RaisedButton(
                              shape: shapeButton,
                              padding: EdgeInsets.all(10),
                              color: Colors.white,
                              child: Text(
                                "Enter",
                                textScaleFactor: 1.4,
                                style: TextStyle(color: Colors.blueGrey),
                              ),
                              onPressed: () {
                                //_showDialog();
                                loginRequest(context);
                              },
                            ),
                          ),
                          GestureDetector(
                            child: Container(
                              padding: EdgeInsets.only(bottom: 20),
                              child: Text(
                                "Create new account",
                                style: TextStyle(color: Colors.white),
                                textScaleFactor: 1.2,
                              ),
                            ),
                            onTap: () => Navigator.pushNamed(context, "/register"),
                          ),
                          /* Container(
                              padding: EdgeInsets.all(12),
                              width: 60,
                              //color: Colors.white,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(60)),
                                color: Colors.white,
                                //border: Border.all(width: 3,color: Colors.green,style: BorderStyle.solid)
                              ),
                              child: Image(
                                  image: AssetImage("assets/images/google.png"))),
                          Container(
                            padding: EdgeInsets.only(bottom: 20),
                            child: Text(
                              "Google account",
                              style: TextStyle(color: Colors.white),
                              textScaleFactor: 1.2,
                            ),
                          ), */
                          Container(
                            padding: EdgeInsets.only(top: 20),
                            child: Text(
                              " V 1.0",
                              style: TextStyle(color: Colors.white),
                              textScaleFactor: 1.2,
                            ),
                          ),
                        ],
                      ),
                    )
                  ),
                ],
              )
            ],
          ),
        )
      ),
    );
  }

  void _showDialog(String message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: Center(
              child: Text(
            "Alert",
            style: TextStyle(color: hexToColor('#FF6060')),
            textScaleFactor: 1.3,
          )),
          content: Container(
            width: 100,
            height: 130,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Center(
                    child: Text(
                  message,
                  textAlign: TextAlign.center,
                )),
                Container(
                  padding: EdgeInsets.all(20),
                  width: 150,
                  //height: 100,
                  child: RaisedButton(
                    shape: shapeButton,
                    padding: EdgeInsets.all(10),
                    color: Colors.green,
                    child: Text(
                      "Ok",
                      textScaleFactor: 1.4,
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void loginRequest(BuildContext context) async {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    if (usernameController.text == "" || passwordController.text == "") {
      _showDialog("All fields should be filled ");
    } else {
      try {
        Map responseData;
        Map userData = {
          "username": "${usernameController.text}",
          "password": "${passwordController.text}",
        };
        Map<String, String> headers = {
          "Content-type": "application/x-www-form-urlencoded",
          "token": "a1b2c3d4e5"
        };
        http.Response response = await http.post(
            'http://usoluweb.company/pdtest/apiparatruel/web/users/validate',
            headers: headers,
            body: userData);
        responseData = json.decode(response.body);
        print("Response: " + responseData.toString());
        if (responseData['success'] == true) {
          //Navigator.pushReplacementNamed(context, '/truelayer');
          //print("success");
          Navigator.of(context).pushNamedAndRemoveUntil('/truelayer', (Route<dynamic> route) => false);
        } else {
          _showDialog("Invalid username or password");
        }
      } catch (e) {
        _showDialog("Something was wrong, try again");
      }
      //_showDialog("Hola: " + responseData['items']['nombre'].toString());
      //Navigator.pushReplacementNamed(context, '/truelayer');
    }
  }
}
