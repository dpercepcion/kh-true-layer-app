import 'dart:async';
import 'package:bank/traits/other_services.dart';
import 'package:bank/traits/user_data.dart';
import 'package:bank/widgets/loader.dart';
import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  Profile() : super();
  static String cardId = "";
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  Map dataClient;
  Map profileList;
  BuildContext globalContext;
  bool loaderState = true;
  Color hexToColor(String code) {
    // convert hexa color to flutter color
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  Future fetchPersonalInfo() async {
    dataClient = await new UserData().getData();
    profileList =
        await new OtherServices().getProfileInfo(dataClient['access_token']);
    loaderState = false;
  }

  @override
  Widget build(BuildContext context) {
    globalContext = context;
    return Container(
      child: Scaffold(
          appBar: AppBar(
            title: Text("Profile"),
            backgroundColor: hexToColor('#FF6060'),
          ),
          body: FutureBuilder(
            future: fetchPersonalInfo(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (this.loaderState == false) {
                return showProfile();
              }
              return Loader();
            },
          )),
    );
  }

  Widget showProfile() {
    return Container(
        padding: EdgeInsets.all(5),
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Name",
                    textScaleFactor: 1.2,
                  ),
                  Text(
                    "${this.profileList['full_name']}",
                    textScaleFactor: 1.2,
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Address",
                    textScaleFactor: 1.1,
                  ),
                  Text(
                    "",
                    textScaleFactor: 1.1,
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Phone",
                    textScaleFactor: 1.1,
                  ),
                  Text(
                    "",
                    textScaleFactor: 1.1,
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
