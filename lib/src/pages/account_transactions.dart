import 'dart:async';
import 'package:bank/traits/user_data.dart';
import 'package:bank/widgets/loader.dart';
import 'package:flutter/material.dart';
import 'package:bank/traits/account_services.dart';
class AccountTransactions extends StatefulWidget {
  AccountTransactions({
    Key key,
  }) : super(key: key);
  static String accountId = "";
  @override
  _AccountTransactionsState createState() => _AccountTransactionsState();
}

class _AccountTransactionsState extends State<AccountTransactions> {

  Map dataClient;
  List accountTransactions;
  Map accountBalance;
  BuildContext globalContext;
  bool loaderState = true;
  Color hexToColor(String code) {
    // convert hexa color to flutter color
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  Future fetchAccountTransactions() async{
    dataClient = await new UserData().getData();
    accountTransactions = await new AccountServices().getAccountTransactions(dataClient['access_token'], AccountTransactions.accountId);
    accountBalance = await new AccountServices().getAccountBalance(dataClient['access_token'], AccountTransactions.accountId);
    loaderState = false;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
       child: Scaffold(
        appBar: AppBar(
          title: Text("Transactions"),
          backgroundColor: hexToColor('#FF6060'),
        ),
        body: FutureBuilder(
        future: fetchAccountTransactions(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (this.loaderState == false) {
            return listAccountTransactions();
          }
          return Loader();
        },
      )
      ),
    );
  }

  Widget listAccountTransactions(){
    return Container(
      padding: EdgeInsets.all(5),
      child: ListView.builder(
          itemCount: this.accountTransactions == null ? 0 : this.accountTransactions.length,
          itemBuilder: (BuildContext context, int index){
            return Column(
              children: <Widget>[
                Card(
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("${this.accountTransactions[index]['timestamp'].substring(0,10)}",
                              textScaleFactor: 1.2,
                            ),
                            Text("${this.accountTransactions[index]['transaction_type']}",
                              textScaleFactor: 1.2,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("${this.accountTransactions[index]['description']}",
                              textScaleFactor: 1.2,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text("${this.accountTransactions[index]['currency']} ${this.accountTransactions[index]['amount']}", 
                              style: TextStyle(
                                color: (this.accountTransactions[index]['amount'] > 0) ? Colors.green: Colors.red
                              ),
                              textScaleFactor: 1.3,
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
                )
              ],
            );
          }
        )
    );
  }
}