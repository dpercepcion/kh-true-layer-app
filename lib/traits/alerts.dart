import 'dart:async';
import 'package:awesome_loader/awesome_loader.dart';
import 'package:flutter/material.dart';

class Alerts{

  Color hexToColor(String code) {
    // convert hexa color to flutter color
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }
  void showDialogLoader(BuildContext context, int durationInSeconds) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        Timer(Duration(seconds: durationInSeconds), () {
          Navigator.of(context).pop();
        });
        // return object of type Dialog
        return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            title: Center(
                child: Text(
              "Loading",
              style: TextStyle(color: hexToColor('#FF6060')),
              textScaleFactor: 1.3,
            )),
            content: Container(
              width: 150,
              height: 150,
              child: Center(
                child: AwesomeLoader(
                  loaderType: AwesomeLoader.AwesomeLoader2,
                  color: Colors.blue,
                ),
              ),
            ));
      },
    );
  }
}